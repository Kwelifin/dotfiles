"{{{ PLUGINS
  call plug#begin('~/.local/share/nvim/plugged')

  " Theme
  Plug 'gosukiwi/vim-atom-dark'

  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'

  Plug 'easymotion/vim-easymotion'
  Plug 'ericbn/vim-relativize'

  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

  "Plug 'ycm-core/YouCompleteMe', { 'do': './install.py' }
  Plug 'lifepillar/vim-mucomplete'
  Plug 'jpalardy/vim-slime'

  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  Plug 'Shougo/neosnippet.vim'
  Plug 'Shougo/neosnippet-snippets'

  "Plug 'neoclide/coc.nvim', {'branch': 'release'}
  call plug#end()
"}}}

"{{{ GENERAL CONFIGS
  set nocompatible
  set hidden
  syntax on
  filetype plugin on
  set completeopt=longest,menuone,noinsert
  "set completeopt=noselect
  set shortmess+=c   " Shut off completion messages
  set belloff+=ctrlg " If Vim beeps during completion
  set number relativenumber
  set foldmethod=marker
  colorscheme atom-dark-256
  set cmdheight=2
  set updatetime=300
  set listchars=tab:>-,trail:~,extends:>
  set list

  let &colorcolumn=join(range(81,999),",")
  highlight ColorColumn ctermbg=235 guibg=#2c2d27
  let &colorcolumn="80,".join(range(120,999),",")

  " Spaces & Tabs {{{
  set tabstop=2       " number of visual spaces per TAB
  set softtabstop=2   " number of spaces in tab when editing
  set shiftwidth=2    " number of spaces to use for autoindent
  set expandtab       " tabs are space
  set autoindent
  set copyindent      " copy indent from the previous line
  " }}} Spaces & Tabs

  let g:EasyMotion_skipfoldedline = 0 " enable jumps to fold groups
  let g:airline_theme='ayu_dark'
  let g:ycm_global_ycm_extra_conf = "~/.vim/bundle/YouCompleteMe/cpp/ycm/.ycm_extra_conf.py"
  let g:syntastic_ocaml_checkers = ['merlin']
  let g:mucomplete#enable_auto_at_startup = 1
  let g:mucomplete#completion_delay = 1
  let g:deoplete#enable_at_startup = 1
  let g:neosnippet#snippets_directory='~/.snippets/'
"}}}

"{{{ KEYBIND
  let mapleader=" "

  imap jj <Esc>
  imap ff <Esc>

  map <Leader>g ^
  map <Leader>h $

  inoremap <C-g> <C-o>^
  inoremap <C-h> <C-o>$

  nnoremap <Leader>W :w<CR>:so %<CR>
  nnoremap <Leader>w :w<CR>
  nnoremap <Leader>Q :q!
  nnoremap <Leader>q :x<CR>
  " Replace tabs to spaces
  nnoremap <Leader>rt :set et\|retab<CR>

  inoremap <expr> <C-j> pumvisible() ? '<C-n>' : '<C-j>'
  inoremap <expr> <C-k> pumvisible() ? '<C-p>' : '<C-k>'

  " easymotion
  map <Leader>s <Plug>(easymotion-prefix)
  map <Leader>sJ <Plug>(easymotion-eol-j)
  map <Leader>sK <Plug>(easymotion-eol-k)
  map <Leader>sd <Plug>(easymotion-bd-w)
  map <Leader>sf <Plug>(easymotion-bd-e)

  " fzf
  nnoremap <Leader>og :GFiles<CR>
  nnoremap <Leader>oo :Files<CR>

  " omnicomplete
  inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
    \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
  inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
    \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

  " neosnippet
  imap <C-l>     <Plug>(neosnippet_expand_or_jump)
  smap <C-l>     <Plug>(neosnippet_expand_or_jump)
  xmap <C-l>     <Plug>(neosnippet_expand_target)

"}}}

"{{{ MERLIN
" ## added by OPAM user-setup for vim / base ## 93ee63e278bdfc07d1139a748ed3fff2 ## you can edit, but keep this line
let s:opam_share_dir = system("opam config var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_check_cmdline = ["opam list --installed --short --safe --color=never"] + s:opam_packages
let s:opam_available_tools = split(system(join(s:opam_check_cmdline)))
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if count(s:opam_available_tools, tool) > 0
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
"}}}
